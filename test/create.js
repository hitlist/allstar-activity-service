'use strict'

const tap = require('tap')

if (!process.env.NODE_ENV === 'test') return tap.pass('skipping test suite')

const uuid = require('uuid')
const {setup, teardown, testCase, models} = require('./helpers')
const test = tap.test

test('activity::create', (t) => {
  const state = {
    hemera: null
  }
  testCase(t, {
    name: 'setup'
  , description: 'common setup'
  }, (tt) => {
    setup().then((hemera) => {
      tt.pass('hemera ready')
      state.hemera = hemera
      tt.end()
    }).catch(tt.error)
  })

  testCase(t, {
    name: 'success'
  , description: 'activity entry'
  }, (tt) => {
    state.hemera.act({
      action: 'create'
    , timeout: 5000
    , topic: 'activity'
    , cmd: 'create'
    , version: 'v1'
    , content_type: 'user'
    , content_id: uuid.v4()
    , service_topic: 'sockets'
    , service_cmd: 'push'
    , service_version: 'v1'
    , creator_type: 'user'
    , creator_id: uuid.v4()
    , extra:  {values: [1, 2, 3]}
    , season: 2017
    , channel: 'test'
    }, (err, res) => {
      tt.error(err)
      tt.match(res, {
      creator_type: 'user'
      , extra: {values: [1, 2, 3]}
      })
      tt.end()
    })
  })

  t.end()
})

test('activity::teardown', (t) => {
  t.pass('teardown')
  teardown(t).catch(t.error).then(() => {
    t.comment('end')
    t.end()
    process.exit(0)
  })
})
