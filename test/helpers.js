'use strict'

const tap = require('tap')
const db = require('zim/lib/db').connection.default
const services = require('../lib/fs/loading/service')


if (require.main === module) return

module.exports = {
  cleardb
, setup
, testCase
, teardown
, db
}

function testCase(t, opts, fn) {
  t.test(`(${opts.name}) - ${opts.description}`, fn)
}

function setup() {
  return new Promise((resolve, reject) => {
    services.load()
    const hemera = require('../lib/hemera')
    hemera.ready(() => {
      resolve(hemera)
    })
  })
}


async function cleardb() {
  await db.query({
    text: 'truncate allstar_activity_entry'
  , values: []
  })
  db.close()
}

function teardown(t) {
  return new Promise((resolve, reject) => {
    const hemera = require('../lib/hemera')
    t.comment('closing hemera')
    cleardb()
      .then(() => {
        t.comment('db closed')
        hemera.close((err) => {
          if (err) return reject(err)
          t.comment(`hemera closed ${!hemera._nats.connected}`)
          resolve(null)
        })
      })
      .catch(t.error)
  })
}
