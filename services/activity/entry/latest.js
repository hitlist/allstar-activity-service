'use strict'

const PGQuery = require('@allstar/pg-query')
const db = require('zim/lib/db').connection.default
const hemera = require('../../../lib/hemera')

const LATEST_QUERY = `
SELECT
  action
, created_at
, content_type
, content_id
, creator_type
, creator_id
, extra
FROM allstar_activity_entry
WHERE created_at BETWEEN $1::DATE AND $1::DATE + INTERVAL '1 YEAR'
ORDER BY created_at DESC
LIMIT 500
`

module.exports =
hemera.add({
  topic: 'activity'
, cmd: 'latest'
, version: 'v1'
}, async function(req) {
  const now = new Date()
  const season = (parseInt(req.season, 10) || now.getFullYear())
  const range = `${season}-01-01`
  const res = await db.query({
    text: LATEST_QUERY
  , values:[
      range
    ]
  })

  this.delegate$.latest = `${range} + 1year`
  return {data: res.rows}
})
