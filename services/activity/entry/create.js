'use strict'

const hemera = require('../../../lib/hemera')
const db = require('zim/lib/db').connection.default
const INSERT_QUERY = `
INSERT INTO allstar_activity_entry (
  action
, content_type
, content_id
, extra
, creator_type
, creator_id
) VALUES (
  $1, $2, $3
, $4, $5, $6
)
RETURNING
  action
, created_at
, allstar_activity_entry_id
, content_type
, content_id
, extra
, creator_type
, creator_id
`
module.exports =
hemera.add({
  topic: 'activity'
, cmd: 'create'
, version: 'v1'
}, async function(req) {
  this.delegate$.action = `${req.action} ${req.content_type}`
  const e = await db.queryOne({
    text: INSERT_QUERY
  , values: [
      req.action
    , req.content_type
    , req.content_id
    , req.extra || {}
    , req.creator_type
    , req.creator_id
    ]
  , name: 'inserty activity record'
  })
  this.log.debug('activity recorded', e.allstar_activity_entry_id)
  this.delegate$.activity = e.allstar_activity_entry_id
  return e
})
