'use strict'

module.exports = {
  "activity": {
    "databases": {
      "allstar": {
        host: "0.0.0.0"
      , user: 'allstar'
      , password: 'abc123'
      , db: "allstar"
      , default: true
      , driver: "postgres"
      , port: 5432
      , pool: {
          min: 5
        , max: 20
        , idleTimeoutMillis: 10000
        }
      }
    }
  }
, "nats":{
    "user": null
  , "pass": null
  , "servers": [
      "nats://0.0.0.0:4222"
    , "nats://0.0.0.0:4223"
    , "nats://0.0.0.0:4224"
    ]
  }
}
