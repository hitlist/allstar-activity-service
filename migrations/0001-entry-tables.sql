CREATE TABLE IF NOT EXISTS allstar_activity_entry (
  allstar_activity_entry_id UUID NOT NULL DEFAULT uuid_generate_v4()
, created_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
, action TEXT NOT NULL
, content_type TEXT NOT NULL
, content_id UUID NOT NULL
, extra JSONB NULL DEFAULT NULL
, creator_type text NOT NULL
, creator_id UUID NOT NULL
);

SELECT create_hypertable('allstar_activity_entry', 'created_at', 'content_type', 4);
CREATE INDEX allstar_activity_created_at_bidx on allstar_activity_entry USING BRIN(created_at);
CREATE INDEX content_type_id_idx ON allstar_activity_entry (created_at DESC, content_type, content_id);
