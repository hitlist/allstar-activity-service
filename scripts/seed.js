'use strict'
const uuid = require('uuid')
const hemera = require('../lib/hemera')
const Entry = require('../services/activity/models/entry')

const types = cycle(['user', 'team'])
const ids = cycle([
  uuid.v4()
, uuid.v4()
, uuid.v4()
, uuid.v4()
])

const extras = cycle([
  'updated his profile'
, {
    a: 1
  , b: 2
  , c: 3
  }
, [1, 2, 3]
, null
])
async function loop() {
  const data = []
  for (let i = 0; i < 10000; i++) {
  hemera.act({
      action: 'create'
    , timeout: 5000
    , pubsub$: true
    , topic: 'activity'
    , cmd: 'create'
    , version: 'v1'
    , content_type: 'user'
    , content_id: uuid.v4()
    , service_topic: 'sockets'
    , service_cmd: 'push'
    , service_version: 'v1'
    , creator_type: types.next().value
    , creator_id: ids.next().value
    , extra:  extras.next().value
    , season: 2017
    , channel: 'test'
    })
  }
}
hemera.ready(() => {
  loop()
})

function* cycle(iter) {
  let idx = -1
  while( true ){
    idx = (idx + 1 ) % iter.length
    yield iter[idx]
  }
}
