'use strict'

const hemera = require('../lib/hemera')

hemera.ready(() => {
  hemera.act({
    topic: 'activity'
  , cmd: 'latest'
  , version: 'v1'
  }, function(err, res) {
    if (err) return console.error(err)
    this.log.info(`${res.length} entries returned`)
  })
})
