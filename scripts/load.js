'use strict'

const uuid = require('uuid/v4')
const hemera = require('./lib/hemera')
const sys_user = '00000000-0000-0000-0000-000000000000'

hemera.ready(async () => {
  let batches = 100
  const types = cycle(['team', 'user', 'conference', 'league', 'game'])
  const ids = cycle([
    uuid()
  , uuid()
  , uuid()
  , uuid()
  , uuid()
  , uuid()
  , uuid()
  ])
  while(batches--) {
    const records = []
    for (let idx = 0; idx < 5000; idx++) {
      records.push(
        hemera.act({
          topic: 'activity'
        , action: 'create'
        , cmd: 'create'
        , version: 'v1'
        , pubsub$: true
        , meta$: {
            user: {superuser: true}
          }
        , content_type: types.next().value
        , content_id: ids.next().value
        , creator_type: 'user'
        , creator_id: sys_user
        })
      )
    }

    const res = await Promise.all(records)
    console.log(batches, 'batches left')
  }
})

function* cycle(items) {
  let idx = -1
  while(true) {
    idx = (idx + 1) % items.length
    yield items[idx]
  }
}
